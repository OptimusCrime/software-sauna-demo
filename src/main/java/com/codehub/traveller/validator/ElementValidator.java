package com.codehub.traveller.validator;

import com.codehub.traveller.model.Element;

/**
 * @author josip
 */
public final class ElementValidator {

    private ElementValidator() {
    }

    /**
     * Returns true if the element has valid horizontal value.
     *
     * @param element element
     * @return boolean
     */
    public static boolean isHorizontalPath(final Element element) {
        return element != null && element.getValue() == '-';
    }

    /**
     * Returns true if the element has valid vertical value.
     *
     * @param element element
     * @return boolean
     */
    public static boolean isVerticalPath(final Element element) {
        return element != null && element.getValue() == '|';
    }

    /**
     * Returns true if the element has valid turn value.
     *
     * @param element element
     * @return boolean
     */
    public static boolean isTurn(final Element element) {
        return element != null && element.getValue() == '+';
    }

    /**
     * Returns true if the element has valid start value.
     *
     * @param element element
     * @return boolean
     */
    public static boolean isStart(final Element element) {
        return element != null && element.getValue() == '@';
    }

    /**
     * Returns true if the element has valid finish value.
     *
     * @param element element
     * @return boolean
     */
    public static boolean isFinish(final Element element) {
        return element != null && element.getValue() == 'x';
    }

    /**
     * Returns true if the element has valid letter value.
     *
     * @param element element
     * @return boolean
     */
    public static boolean isLetter(final Element element) {
        return element != null && element.getValue() >= 'A' && element.getValue() <= 'Z';
    }

    /**
     * Returns true if element is valid element.
     *
     * @param element element
     * @return boolean
     */
    public static boolean isValidElement(final Element element) {
        return isHorizontalPath(element)
                || isVerticalPath(element)
                || isTurn(element)
                || isStart(element)
                || isFinish(element)
                || isLetter(element);
    }
}
