package com.codehub.traveller.validator;

import com.codehub.traveller.exception.InvalidPathException;
import com.codehub.traveller.model.Direction;

import java.util.List;

/**
 * @author josip
 */
public final class DirectionValidator {
    private DirectionValidator() {
    }

    /**
     * Validates direction candidates.
     *
     * @param directionCandidates list of potential directions
     */
    public static void validateDirectionCandidates(final List<Direction> directionCandidates) {
        if (directionCandidates == null || directionCandidates.isEmpty()) {
            throw new InvalidPathException("No valid paths found!");
        }

        if (directionCandidates.size() > 1) {
            throw new InvalidPathException("Multiple paths found");
        }
    }
}
