package com.codehub.traveller.validator;

import com.codehub.traveller.model.Element;

/**
 * @author josip
 */
public final class StepValidator {

    private StepValidator() {
    }

    /**
     * Returns true if the element is valid vertical step.
     *
     * @param element element
     * @return boolean
     */
    public static boolean isValidVerticalStep(final Element element) {
        return isValidStep(element) || ElementValidator.isVerticalPath(element);
    }

    /**
     * Returns true if the element is valid horizontal step.
     *
     * @param element element
     * @return boolean
     */
    public static boolean isValidHorizontalStep(final Element element) {
        return isValidStep(element) || ElementValidator.isHorizontalPath(element);
    }

    private static boolean isValidStep(final Element element) {
        return ElementValidator.isFinish(element)
                || ElementValidator.isLetter(element)
                || ElementValidator.isTurn(element);
    }
}
