package com.codehub.traveller;

import com.codehub.traveller.factory.MatrixFactory;
import com.codehub.traveller.handler.InputHandler;
import com.codehub.traveller.model.Matrix;
import com.codehub.traveller.model.TravelResult;
import com.codehub.traveller.travel.PathTraveller;

/**
 * @author josip
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String... args) {
        String[] inputs = InputHandler.getMultipleInputMapsFromFile("inputs/maps.txt");
        for (String input : inputs) {
            Matrix matrix = new MatrixFactory().create(input);
            TravelResult travelResult = new PathTraveller(matrix)
                    .initialize()
                    .travel();
            displayResult(travelResult);
        }
    }

    /**
     * Outputs collected letters and path taken.
     *
     * @param travelResult Matrix Traveller
     */
    private static void displayResult(final TravelResult travelResult) {
        System.out.println("Collected letters: " + travelResult.getCollectedLetters());
        System.out.println("Path taken: " + travelResult.getPath());
    }
}
