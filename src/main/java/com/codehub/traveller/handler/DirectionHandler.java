package com.codehub.traveller.handler;

import com.codehub.traveller.exception.InvalidDirectionException;
import com.codehub.traveller.exception.InvalidElementException;
import com.codehub.traveller.exception.MissingStartException;
import com.codehub.traveller.model.Coordinates;
import com.codehub.traveller.model.Direction;
import com.codehub.traveller.model.Element;
import com.codehub.traveller.model.Matrix;
import com.codehub.traveller.validator.DirectionValidator;

import java.util.ArrayList;
import java.util.List;

import static com.codehub.traveller.resolver.CoordinatesResolver.*;
import static com.codehub.traveller.validator.StepValidator.isValidHorizontalStep;
import static com.codehub.traveller.validator.StepValidator.isValidVerticalStep;

/**
 * @author josip
 */
public class DirectionHandler {

    private final Matrix matrix;

    /**
     * Default constructor.
     *
     * @param matrix matrix
     */
    public DirectionHandler(final Matrix matrix) {
        this.matrix = matrix;
    }

    /**
     * Determines initial direction an the start ot the pathing.
     *
     * @return direction
     */
    public Direction findInitialDirection() {
        if (matrix.getStart() == null) {
            throw new MissingStartException();
        }

        List<Direction> directionCandidates = new ArrayList<>();

        if (resolveLeft(matrix.getStart().getCoordinates())) {
            directionCandidates.add(Direction.LEFT);
        }

        if (resolveRight(matrix.getStart().getCoordinates())) {
            directionCandidates.add(Direction.RIGHT);
        }

        if (resolveUpper(matrix.getStart().getCoordinates())) {
            directionCandidates.add(Direction.UP);
        }

        if (resolveDownwards(matrix.getStart().getCoordinates())) {
            directionCandidates.add(Direction.DOWN);
        }

        DirectionValidator.validateDirectionCandidates(directionCandidates);
        return directionCandidates.get(0);
    }

    /**
     * Resolves the turn in the path.
     *
     * @param currentElement   current element in the matrix
     * @param currentDirection current direction before turn
     */
    public Direction resolveTurn(final Element currentElement, final Direction currentDirection) {
        if (currentElement == null) {
            throw new InvalidElementException();
        }
        if (currentDirection == null) {
            throw new InvalidDirectionException();
        }
        List<Direction> directionCandidates = new ArrayList<>();
        switch (currentDirection) {
            case RIGHT:
            case LEFT:
                if (resolveUpper(currentElement.getCoordinates())) {
                    directionCandidates.add(Direction.UP);
                }

                if (resolveDownwards(currentElement.getCoordinates())) {
                    directionCandidates.add(Direction.DOWN);
                }
                break;
            case UP:
            case DOWN:
                if (resolveLeft(currentElement.getCoordinates())) {
                    directionCandidates.add(Direction.LEFT);
                }

                if (resolveRight(currentElement.getCoordinates())) {
                    directionCandidates.add(Direction.RIGHT);
                }
                break;
        }
        DirectionValidator.validateDirectionCandidates(directionCandidates);
        return directionCandidates.get(0);
    }

    private boolean resolveLeft(final Coordinates coordinates) {
        Element left = matrix.getElementByCoordinates(getLeftAdjacentElementCoordinates(coordinates));
        return left != null && isValidHorizontalStep(left);
    }

    private boolean resolveRight(final Coordinates coordinates) {
        Element right = matrix.getElementByCoordinates(getRightAdjacentElementCoordinates(coordinates));
        return right != null && isValidHorizontalStep(right);
    }

    private boolean resolveUpper(final Coordinates coordinates) {
        Element upper = matrix.getElementByCoordinates(getUpperAdjacentElementCoordinates(coordinates));
        return upper != null && isValidVerticalStep(upper);
    }

    private boolean resolveDownwards(final Coordinates coordinates) {
        Element downwards = matrix.getElementByCoordinates(getDownwardsAdjacentElementCoordinates(coordinates));
        return downwards != null && isValidVerticalStep(downwards);
    }
}
