package com.codehub.traveller.handler;

import com.codehub.traveller.exception.InputFileNotFoundException;
import com.codehub.traveller.exception.InvalidFileException;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;

/**
 * @author josip
 */
public final class InputHandler {

    private InputHandler() {
    }

    /**
     * Returns String representation of input file map.
     *
     * @param filename name of the input file
     * @return input file map as String
     */
    public static String getInputMapFromFile(final String filename) {
        if (filename == null) {
            throw new InputFileNotFoundException("File name is null");
        }
        String input;
        try {
            input = new String(Files.readAllBytes(getFileFromResource(filename).toPath()));
        } catch (IOException e) {
            throw new InvalidFileException("Error while reading file.", e);
        }
        return input;
    }

    /**
     * Returns multiple String representation of input file maps.
     *
     * @param filename name of the input file
     * @return input file maps as strings
     */
    public static String[] getMultipleInputMapsFromFile(final String filename) {
        if (filename == null) {
            throw new InputFileNotFoundException("File name is null");
        }
        String input = getInputMapFromFile(filename);
        return input.split(",");
    }

    /**
     * Fetches the input file from resources folder.
     *
     * @param filename name of the input file.
     * @return input file
     */
    private static File getFileFromResource(final String filename) {
        ClassLoader classLoader = InputHandler.class.getClassLoader();
        URL resource = classLoader.getResource(filename);
        if (resource == null) {
            throw new InputFileNotFoundException();
        } else {
            return new File(resource.getFile());
        }
    }
}
