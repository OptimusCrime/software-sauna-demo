package com.codehub.traveller.factory;

import com.codehub.traveller.exception.*;
import com.codehub.traveller.model.Coordinates;
import com.codehub.traveller.model.Element;
import com.codehub.traveller.model.Matrix;
import com.codehub.traveller.validator.ElementValidator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author josip
 */
public class MatrixFactory {

    /**
     * Creates instance of Matrix.
     *
     * @param input map as String
     * @return matrix
     */
    public Matrix create(String input) {
        if (input == null) {
            throw new InvalidInputMapException();
        }
        List<Element> elements = new ArrayList<>();
        List<Element> starts = new ArrayList<>();
        List<Element> finishes = new ArrayList<>();
        String[] rows = input.split("\\r?\\n");

        for (int i = 0; i < rows.length; i++) {
            char[] rowElements = rows[i].toCharArray();
            for (int j = 0; j < rowElements.length; j++) {
                Element currentElement = new Element(new Coordinates(i, j), rowElements[j]);
                if (ElementValidator.isValidElement(currentElement)) {
                    elements.add(currentElement);
                }

                if (ElementValidator.isStart(currentElement)) {
                    starts.add(currentElement);
                }

                if (ElementValidator.isFinish(currentElement)) {
                    finishes.add(currentElement);
                }
            }
        }

        if (finishes.size() > 1) {
            throw new MultipleFinishException();
        }

        if (starts.size() > 1) {
            throw new MultipleStartException();
        }
        return new Matrix(elements,
                starts.stream().findFirst().orElseThrow(MissingStartException::new),
                finishes.stream().findFirst().orElseThrow(MissingFinishException::new));
    }
}
