package com.codehub.traveller.model;

/**
 * @author josip
 */
public enum Direction {
    UP, DOWN, LEFT, RIGHT;
}
