package com.codehub.traveller.model;

/**
 * @author josip
 */
public class Element {
    private Coordinates coordinates;
    private char value;

    public Element(Coordinates coordinates, char value) {
        this.coordinates = coordinates;
        this.value = value;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public char getValue() {
        return value;
    }

    public void setValue(char value) {
        this.value = value;
    }
}
