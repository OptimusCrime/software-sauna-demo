package com.codehub.traveller.model;

import com.codehub.traveller.exception.InvalidCoordinatesException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author josip
 */
public class TravelResult {
    private List<Coordinates> visited = new ArrayList<>();
    private List<Character> path = new ArrayList<>();
    private List<Character> collectedLetters = new ArrayList<>();

    public List<Coordinates> getVisited() {
        return visited;
    }

    public void addToVisited(final Coordinates coordinates)
    {
        if (coordinates == null) {
            throw new InvalidCoordinatesException();
        }
        this.visited.add(coordinates);
    }

    public List<Character> getPath() {
        return path;
    }

    public void addToPath(final char charValue) {
        this.path.add(charValue);
    }

    public List<Character> getCollectedLetters() {
        return collectedLetters;
    }

    public void addToCollectedLetters(final char letter)
    {
        this.collectedLetters.add(letter);
    }
}
