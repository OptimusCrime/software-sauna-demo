package com.codehub.traveller.model;

import com.codehub.traveller.exception.InvalidDirectionException;
import com.codehub.traveller.exception.InvalidElementException;

import java.util.List;

import static com.codehub.traveller.resolver.CoordinatesResolver.*;

/**
 * @author josip
 */
public class Matrix {
    private final List<Element> elements;
    private final Element start;
    private final Element finish;

    public Matrix(List<Element> elements, Element start, Element finish) {
        this.elements = elements;
        this.start = start;
        this.finish = finish;
    }

    public List<Element> getElements() {
        return elements;
    }

    public Element getStart() {
        return start;
    }

    public Element getFinish() {
        return finish;
    }

    /**
     * Returns element at given coordinates location.
     *
     * @param coordinates element coordinates
     * @return element at coordinates location.
     */
    public Element getElementByCoordinates(final Coordinates coordinates) {
        return this.elements
                .stream()
                .filter(element -> element.getCoordinates().getxAxis() == coordinates.getxAxis()
                        && element.getCoordinates().getyAxis() == coordinates.getyAxis())
                .findFirst()
                .orElse(null);
    }

    /**
     * Returns adjacent element based on given direction.
     *
     * @param direction      direction
     * @param currentElement current element
     * @return adjacent element
     */
    public Element getElementForDirection(final Direction direction, final Element currentElement) {
        if (currentElement == null) {
            throw new InvalidElementException();
        }
        if (direction == null) {
            throw new InvalidDirectionException();
        }
        switch (direction) {
            case UP:
                return this.getElementByCoordinates(getUpperAdjacentElementCoordinates(currentElement.getCoordinates()));
            case DOWN:
                return this.getElementByCoordinates(getDownwardsAdjacentElementCoordinates(currentElement.getCoordinates()));
            case LEFT:
                return this.getElementByCoordinates(getLeftAdjacentElementCoordinates(currentElement.getCoordinates()));
            case RIGHT:
                return this.getElementByCoordinates(getRightAdjacentElementCoordinates(currentElement.getCoordinates()));
            default:
                throw new InvalidDirectionException("Unexpected value: " + direction);
        }
    }
}
