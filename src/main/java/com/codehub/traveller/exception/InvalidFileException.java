package com.codehub.traveller.exception;

/**
 * @author josip
 */
public class InvalidFileException extends RuntimeException {
    public InvalidFileException() {
        super("Invalid file!");
    }

    public InvalidFileException(String message) {
        super(message);
    }

    public InvalidFileException(String message, Throwable cause) {
        super(message, cause);
    }
}
