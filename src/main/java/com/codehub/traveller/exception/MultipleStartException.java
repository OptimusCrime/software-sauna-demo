package com.codehub.traveller.exception;

/**
 * @author josip
 */
public class MultipleStartException extends RuntimeException {
    public MultipleStartException() {
        super("There are multiple starts!");
    }

    public MultipleStartException(String message) {
        super(message);
    }

    public MultipleStartException(String message, Throwable cause) {
        super(message, cause);
    }
}
