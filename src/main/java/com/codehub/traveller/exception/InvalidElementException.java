package com.codehub.traveller.exception;

/**
 * @author josip
 */
public class InvalidElementException extends RuntimeException {
    public InvalidElementException() {
        super("Invalid element!");
    }

    public InvalidElementException(String message) {
        super(message);
    }

    public InvalidElementException(String message, Throwable cause) {
        super(message, cause);
    }
}
