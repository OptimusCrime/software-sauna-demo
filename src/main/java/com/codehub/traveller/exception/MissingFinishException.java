package com.codehub.traveller.exception;

/**
 * @author josip
 */
public class MissingFinishException extends RuntimeException {
    public MissingFinishException() {
        super("Finish is missing!");
    }

    public MissingFinishException(String message) {
        super(message);
    }

    public MissingFinishException(String message, Throwable cause) {
        super(message, cause);
    }
}
