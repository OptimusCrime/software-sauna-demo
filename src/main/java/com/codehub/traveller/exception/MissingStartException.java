package com.codehub.traveller.exception;

/**
 * @author josip
 */
public class MissingStartException extends RuntimeException {
    public MissingStartException() {
        super("Start is missing!");
    }

    public MissingStartException(String message) {
        super(message);
    }

    public MissingStartException(String message, Throwable cause) {
        super(message, cause);
    }
}
