package com.codehub.traveller.exception;

/**
 * @author josip
 */
public class MultipleFinishException extends RuntimeException {
    public MultipleFinishException() {
        super("There are multiple finishes!");
    }

    public MultipleFinishException(String message) {
        super(message);
    }

    public MultipleFinishException(String message, Throwable cause) {
        super(message, cause);
    }
}
