package com.codehub.traveller.exception;

/**
 * @author josip
 */
public class InvalidPathException extends RuntimeException {
    public InvalidPathException() {
        super("Path is invalid!");
    }

    public InvalidPathException(String message) {
        super(message);
    }

    public InvalidPathException(String message, Throwable cause) {
        super(message, cause);
    }
}
