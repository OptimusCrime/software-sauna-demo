package com.codehub.traveller.exception;

/**
 * @author josip
 */
public class InvalidDirectionException extends RuntimeException {
    public InvalidDirectionException() {
        super("Invalid direction!");
    }

    public InvalidDirectionException(String message) {
        super(message);
    }

    public InvalidDirectionException(String message, Throwable cause) {
        super(message, cause);
    }
}
