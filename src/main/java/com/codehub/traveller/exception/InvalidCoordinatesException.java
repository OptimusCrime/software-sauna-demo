package com.codehub.traveller.exception;

/**
 * @author josip
 */
public class InvalidCoordinatesException extends RuntimeException {
    public InvalidCoordinatesException() {
        super("Invalid coordinates!");
    }

    public InvalidCoordinatesException(String message) {
        super(message);
    }

    public InvalidCoordinatesException(String message, Throwable cause) {
        super(message, cause);
    }
}
