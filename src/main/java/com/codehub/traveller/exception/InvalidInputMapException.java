package com.codehub.traveller.exception;

/**
 * @author josip
 */
public class InvalidInputMapException extends RuntimeException {
    public InvalidInputMapException() {
        super("Invalid input map!");
    }

    public InvalidInputMapException(String message) {
        super(message);
    }

    public InvalidInputMapException(String message, Throwable cause) {
        super(message, cause);
    }
}
