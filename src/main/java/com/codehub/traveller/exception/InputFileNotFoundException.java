package com.codehub.traveller.exception;

/**
 * @author josip
 */
public class InputFileNotFoundException extends RuntimeException {
    public InputFileNotFoundException() {
        super("Input file not found!");
    }

    public InputFileNotFoundException(String message) {
        super(message);
    }
}
