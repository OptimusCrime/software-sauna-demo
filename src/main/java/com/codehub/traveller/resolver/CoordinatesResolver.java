package com.codehub.traveller.resolver;

import com.codehub.traveller.exception.InvalidCoordinatesException;
import com.codehub.traveller.model.Coordinates;

/**
 * @author josip
 */
public final class CoordinatesResolver {

    private CoordinatesResolver() {
    }

    /**
     * Returns coordinates of the first left element in the matrix.
     *
     * @param currentPosition current element position
     * @return coordinates
     */
    public static Coordinates getLeftAdjacentElementCoordinates(final Coordinates currentPosition) {
        if (currentPosition == null) {
            throw new InvalidCoordinatesException();
        }
        return new Coordinates(currentPosition.getxAxis(), currentPosition.getyAxis() - 1);
    }

    /**
     * Returns coordinates of the first right element in the matrix.
     *
     * @param currentPosition current element position
     * @return coordinates
     */
    public static Coordinates getRightAdjacentElementCoordinates(final Coordinates currentPosition) {
        if (currentPosition == null) {
            throw new InvalidCoordinatesException();
        }
        return new Coordinates(currentPosition.getxAxis(), currentPosition.getyAxis() + 1);
    }

    /**
     * Returns coordinates of the first upper element in the matrix.
     *
     * @param currentPosition current element position
     * @return coordinates
     */
    public static Coordinates getUpperAdjacentElementCoordinates(final Coordinates currentPosition) {
        if (currentPosition == null) {
            throw new InvalidCoordinatesException();
        }
        return new Coordinates(currentPosition.getxAxis() - 1, currentPosition.getyAxis());
    }

    /**
     * Returns coordinates of the first downwards element in the matrix.
     *
     * @param currentPosition current element position
     * @return coordinates
     */
    public static Coordinates getDownwardsAdjacentElementCoordinates(final Coordinates currentPosition) {
        if (currentPosition == null) {
            throw new InvalidCoordinatesException();
        }
        return new Coordinates(currentPosition.getxAxis() + 1, currentPosition.getyAxis());
    }
}
