package com.codehub.traveller.travel;

import com.codehub.traveller.handler.DirectionHandler;
import com.codehub.traveller.model.Matrix;
import com.codehub.traveller.model.TravelResult;

/**
 * @author josip
 */
public class PathTraveller {

    private final Matrix matrix;

    /**
     * Default constructor.
     *
     * @param matrix matrix
     */
    public PathTraveller(Matrix matrix) {
        this.matrix = matrix;
    }

    /**
     * Initialize starting data for travel.
     *
     * @return instance of Travel
     */
    public Travel initialize() {
        if (matrix == null) {
            throw new IllegalArgumentException("Matrix must not be null!");
        }
        DirectionHandler directionHandler = new DirectionHandler(matrix);
        TravelResult travelResult = new TravelResult();
        travelResult.addToPath(matrix.getStart().getValue());

        return new Travel(
                directionHandler,
                matrix,
                matrix.getStart(),
                directionHandler.findInitialDirection(),
                travelResult);
    }
}
