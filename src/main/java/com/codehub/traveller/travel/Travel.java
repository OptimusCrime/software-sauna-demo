package com.codehub.traveller.travel;

import com.codehub.traveller.exception.InvalidElementException;
import com.codehub.traveller.exception.InvalidPathException;
import com.codehub.traveller.handler.DirectionHandler;
import com.codehub.traveller.model.Direction;
import com.codehub.traveller.model.Element;
import com.codehub.traveller.model.Matrix;
import com.codehub.traveller.model.TravelResult;

import static com.codehub.traveller.validator.ElementValidator.*;

/**
 * @author josip
 */
public class Travel {
    private final DirectionHandler directionHandler;
    private final Matrix matrix;
    private Element currentElement;
    private Direction currentDirection;
    private final TravelResult travelResult;

    /**
     * Default package private constructor.
     *
     * @param directionHandler direction handler
     * @param matrix           matrix
     * @param currentElement   current element
     * @param currentDirection current direction
     * @param travelResult     travel result
     */
    Travel(DirectionHandler directionHandler,
           Matrix matrix,
           Element currentElement,
           Direction currentDirection,
           TravelResult travelResult) {
        this.directionHandler = directionHandler;
        this.matrix = matrix;
        this.currentElement = currentElement;
        this.currentDirection = currentDirection;
        this.travelResult = travelResult;
    }

    /**
     * Travel the matrix and return travel result.
     *
     * @return travel result
     */
    public TravelResult travel() {
        currentElement = matrix.getElementForDirection(currentDirection, currentElement);
        this.validateElement();
        this.processStep();

        if (!isFinish(currentElement)) {
            this.travel();
        }
        return travelResult;
    }

    private void processStep() {
        travelResult.addToPath(currentElement.getValue());

        if (isTurn(currentElement)) {
            currentDirection = directionHandler.resolveTurn(currentElement, currentDirection);
        }

        if (isLetter(currentElement) && !travelResult.getVisited().contains(currentElement.getCoordinates())) {
            travelResult.addToCollectedLetters(currentElement.getValue());
            travelResult.addToVisited(currentElement.getCoordinates());
            if (letterIsTurn()) {
                currentDirection = directionHandler.resolveTurn(currentElement, currentDirection);
            }
        }
    }

    private boolean letterIsTurn() {
        return matrix.getElementForDirection(currentDirection, currentElement) == null;
    }

    private void validateElement() {
        if (currentElement == null) {
            throw new InvalidPathException();
        }

        if (!isValidElement(currentElement)) {
            throw new InvalidElementException();
        }
    }
}
