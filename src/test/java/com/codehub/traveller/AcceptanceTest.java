package com.codehub.traveller;

import com.codehub.traveller.exception.*;
import com.codehub.traveller.factory.MatrixFactory;
import com.codehub.traveller.handler.InputHandler;
import com.codehub.traveller.model.Matrix;
import com.codehub.traveller.model.TravelResult;
import com.codehub.traveller.travel.PathTraveller;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.stream.Collectors;

public class AcceptanceTest {

    private String[] rawMaps;

    private TravelResult travelResult;

    private Matrix matrix;

    private PathTraveller pathTraveller;

    /**
     * Set up.
     */
    @Before
    public void setUp() {
        rawMaps = InputHandler.getMultipleInputMapsFromFile("test/acceptance-test-maps.txt");
    }

    @Test
    public void basicMap() {
        String expectedLetters = "ACB";
        String expectedPath = "@---A---+|C|+---+|+-B-x";
        executeValidTestCase(expectedLetters, expectedPath, rawMaps[0]);
    }

    @Test
    public void straightThroughIntersections() {
        String expectedLetters = "ABCD";
        String expectedPath = "@|A+---B--+|+--C-+|-||+---D--+|x";
        executeValidTestCase(expectedLetters, expectedPath, rawMaps[1]);
    }

    @Test
    public void lettersOnTurns() {
        String expectedLetters = "ACB";
        String expectedPath = "@---A---+|||C---+|+-B-x";
        executeValidTestCase(expectedLetters, expectedPath, rawMaps[2]);
    }

    @Test
    public void noLettersTwice() {
        String expectedLetters = "ABCD";
        String expectedPath = "@--A-+|+-+|A|+--B--+C|+-+|+-C-+|D|x";
        executeValidTestCase(expectedLetters, expectedPath, rawMaps[3]);
    }

    @Test
    public void keepDirection() {
        String expectedLetters = "ABCD";
        String expectedPath = "@A+++A|+-B-+C+++C-+Dx";
        executeValidTestCase(expectedLetters, expectedPath, rawMaps[4]);
    }

    @Test(expected = MissingStartException.class)
    public void noStart() {
        matrix = new MatrixFactory().create(rawMaps[5]);
    }

    @Test(expected = MissingFinishException.class)
    public void noEnd() {
        matrix = new MatrixFactory().create(rawMaps[6]);
    }

    @Test(expected = MultipleStartException.class)
    public void multipleStart() {
        matrix = new MatrixFactory().create(rawMaps[7]);
    }

    @Test(expected = MultipleFinishException.class)
    public void multipleEnd() {
        matrix = new MatrixFactory().create(rawMaps[8]);
    }

    @Test(expected = InvalidPathException.class)
    public void tForks() {
        matrix = new MatrixFactory().create(rawMaps[9]);

        pathTraveller = new PathTraveller(matrix);
        travelResult = pathTraveller.initialize().travel();
    }

    private void executeValidTestCase(String expectedLetters, String expectedPath, String inputMap) {
        matrix = new MatrixFactory().create(inputMap);

        pathTraveller = new PathTraveller(matrix);
        travelResult = pathTraveller.initialize().travel();

        Assert.assertEquals(
                expectedLetters,
                travelResult.getCollectedLetters().stream().map(String::valueOf).collect(Collectors.joining()));

        Assert.assertEquals(
                expectedPath,
                travelResult.getPath().stream().map(String::valueOf).collect(Collectors.joining()));
    }
}