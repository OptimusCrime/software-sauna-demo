package com.codehub.traveller.factory;

import com.codehub.traveller.exception.*;
import com.codehub.traveller.model.Coordinates;
import com.codehub.traveller.model.Element;
import com.codehub.traveller.model.Matrix;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MatrixFactoryTest {

    private MatrixFactory matrixFactory;

    /**
     * Set up.
     */
    @Before
    public void setUp() {
        matrixFactory = new MatrixFactory();
    }

    @Test(expected = InvalidInputMapException.class)
    public void createWithNullInputThrowsInvalidInputMapException() {
        matrixFactory.create(null);
    }

    @Test(expected = MultipleFinishException.class)
    public void createWithMultipleFinishesInInputMapThrowsMultipleFinishException() {
        String input = "@---A---+\n" +
                "        |\n" +
                "xxB-+   C\n" +
                "    |   |\n" +
                "    +---+";
        matrixFactory.create(input);
    }

    @Test(expected = MissingFinishException.class)
    public void createWithNoFinishesInInputMapThrowsMissingFinishException() {
        String input = "@---A---+\n" +
                "        |\n" +
                "--B-+   C\n" +
                "    |   |\n" +
                "    +---+";
        matrixFactory.create(input);
    }

    @Test(expected = MultipleStartException.class)
    public void createWithMultipleStartsInInputMapThrowsMultipleStartException() {
        String input = "@@--A---+\n" +
                "        |\n" +
                "x-B-+   C\n" +
                "    |   |\n" +
                "    +---+";
        matrixFactory.create(input);
    }

    @Test(expected = MissingStartException.class)
    public void createWithNoStartsInInputMapThrowsMissingStartException() {
        String input = "----A---+\n" +
                "        |\n" +
                "x-B-+   C\n" +
                "    |   |\n" +
                "    +---+";
        matrixFactory.create(input);
    }

    @Test
    public void createWithCorrectInputReturnsMatrix() {
        String input = "@---A---+\n" +
                "        |\n" +
                "x-B-+   C\n" +
                "    |   |\n" +
                "    +---+";
        Matrix matrix = matrixFactory.create(input);
        Element start = new Element(new Coordinates(0,0), '@');
        Element finish = new Element(new Coordinates(2,0), 'x');
        Assert.assertEquals(start.getCoordinates().getxAxis(), matrix.getStart().getCoordinates().getxAxis());
        Assert.assertEquals(start.getCoordinates().getyAxis(), matrix.getStart().getCoordinates().getyAxis());
        Assert.assertEquals(finish.getCoordinates().getxAxis(), matrix.getFinish().getCoordinates().getxAxis());
        Assert.assertEquals(finish.getCoordinates().getyAxis(), matrix.getFinish().getCoordinates().getyAxis());
        Assert.assertEquals(23, matrix.getElements().size());
    }
}