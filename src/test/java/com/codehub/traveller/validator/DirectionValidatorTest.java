package com.codehub.traveller.validator;

import com.codehub.traveller.exception.InvalidPathException;
import com.codehub.traveller.model.Direction;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

public class DirectionValidatorTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private List<Direction> directionCandidates;

    @Test
    public void directionCandidatesEqualsNullThrowsInvalidPathException() {
        expectedException.expect(InvalidPathException.class);
        expectedException.expectMessage("No valid paths found!");
        DirectionValidator.validateDirectionCandidates(directionCandidates);
    }

    @Test
    public void noDirectionCandidatesThrowsInvalidPathException() {
        expectedException.expect(InvalidPathException.class);
        expectedException.expectMessage("No valid paths found!");

        directionCandidates = new ArrayList<>();
        DirectionValidator.validateDirectionCandidates(directionCandidates);
    }

    @Test
    public void multipleDirectionCandidatesThrowsInvalidPathException() {
        expectedException.expect(InvalidPathException.class);
        expectedException.expectMessage("Multiple paths found");

        directionCandidates = new ArrayList<>();
        directionCandidates.add(Direction.UP);
        directionCandidates.add(Direction.DOWN);
        DirectionValidator.validateDirectionCandidates(directionCandidates);
    }

    @Test
    public void validDirectionCandidatesInputThrowsNoException() {
        directionCandidates = new ArrayList<>();
        directionCandidates.add(Direction.UP);

        DirectionValidator.validateDirectionCandidates(directionCandidates);
    }
}