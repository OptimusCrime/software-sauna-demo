package com.codehub.traveller.validator;

import com.codehub.traveller.model.Coordinates;
import com.codehub.traveller.model.Element;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.codehub.traveller.validator.ElementValidator.*;

public class ElementValidatorTest {

    private Element element;

    @Mock
    private Coordinates coordinates;

    /**
     * Set up.
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void isHorizontalPathWithNullElementReturnsFalse() {
        Assert.assertFalse(isHorizontalPath(element));
    }

    @Test
    public void isHorizontalPathWithNonHorizontalElementReturnsFalse() {
        element = new Element(coordinates, ' ');
        Assert.assertFalse(isHorizontalPath(element));
    }

    @Test
    public void isHorizontalPathWithValidElementReturnsTrue() {
        element = new Element(coordinates, '-');
        Assert.assertTrue(isHorizontalPath(element));
    }

    @Test
    public void isVerticalPathWithNullElementReturnsFalse() {
        Assert.assertFalse(isVerticalPath(element));
    }

    @Test
    public void isVerticalPathWithNonVerticalElementReturnsFalse() {
        element = new Element(coordinates, ' ');
        Assert.assertFalse(isVerticalPath(element));
    }

    @Test
    public void isVerticalPathWithValidElementReturnsTrue() {
        element = new Element(coordinates, '|');
        Assert.assertTrue(isVerticalPath(element));
    }

    @Test
    public void isTurnPathWithNullElementReturnsFalse() {
        Assert.assertFalse(isTurn(element));
    }

    @Test
    public void isTurnPathWithNonTurnElementReturnsFalse() {
        element = new Element(coordinates, ' ');
        Assert.assertFalse(isTurn(element));
    }

    @Test
    public void isTurnPathWithValidElementReturnsTrue() {
        element = new Element(coordinates, '+');
        Assert.assertTrue(isTurn(element));
    }

    @Test
    public void isStartPathWithNullElementReturnsFalse() {
        Assert.assertFalse(isStart(element));
    }

    @Test
    public void isStartPathWithNonStartElementReturnsFalse() {
        element = new Element(coordinates, ' ');
        Assert.assertFalse(isStart(element));
    }

    @Test
    public void isStartPathWithValidElementReturnsTrue() {
        element = new Element(coordinates, '@');
        Assert.assertTrue(isStart(element));
    }

    @Test
    public void isFinishPathWithNullElementReturnsFalse() {
        Assert.assertFalse(isFinish(element));
    }

    @Test
    public void isFinishPathWithNonFinishElementReturnsFalse() {
        element = new Element(coordinates, ' ');
        Assert.assertFalse(isFinish(element));
    }

    @Test
    public void isFinishPathWithValidElementReturnsTrue() {
        element = new Element(coordinates, 'x');
        Assert.assertTrue(isFinish(element));
    }

    @Test
    public void isLetterPathWithNullElementReturnsFalse() {
        Assert.assertFalse(isLetter(element));
    }

    @Test
    public void isLetterPathWithNonLetterElementReturnsFalse() {
        element = new Element(coordinates, ' ');
        Assert.assertFalse(isLetter(element));
    }

    @Test
    public void isLetterPathWithValidElementReturnsTrue() {
        for (char c = 'A'; c <= 'Z'; c++) {
            element = new Element(coordinates, c);
            Assert.assertTrue(isLetter(element));
        }
    }

    @Test
    public void isValidElementWithNullElementReturnsFalse() {
        Assert.assertFalse(isValidElement(element));
    }

    @Test
    public void isValidElementWithInvalidElementReturnsFalse() {
        element = new Element(coordinates, ' ');
        Assert.assertFalse(isLetter(element));
    }

    @Test
    public void isValidElementWithValidElementReturnsTrue() {
        for (char c = 'A'; c <= 'Z'; c++) {
            element = new Element(coordinates, c);
            Assert.assertTrue(isLetter(element));
        }
        Assert.assertTrue(isValidElement(new Element(coordinates, '-')));
        Assert.assertTrue(isValidElement(new Element(coordinates, '|')));
        Assert.assertTrue(isValidElement(new Element(coordinates, '+')));
        Assert.assertTrue(isValidElement(new Element(coordinates, '@')));
        Assert.assertTrue(isValidElement(new Element(coordinates, 'x')));
    }
}