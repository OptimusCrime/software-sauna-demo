package com.codehub.traveller.validator;

import com.codehub.traveller.model.Coordinates;
import com.codehub.traveller.model.Element;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.codehub.traveller.validator.StepValidator.isValidHorizontalStep;
import static com.codehub.traveller.validator.StepValidator.isValidVerticalStep;

public class StepValidatorTest {

    private Element element;
    @Mock
    private Coordinates coordinates;

    /**
     * Set up.
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void isValidVerticalStepWithNullElementReturnsFalse() {
        Assert.assertFalse(isValidVerticalStep(element));
    }

    @Test
    public void isValidVerticalStepWithNonValidVerticalStepElementReturnsFalse() {
        Assert.assertFalse(isValidVerticalStep(new Element(coordinates, '-')));
        Assert.assertFalse(isValidVerticalStep(new Element(coordinates, ' ')));
        Assert.assertFalse(isValidVerticalStep(new Element(coordinates, '@')));
    }

    @Test
    public void isValidVerticalStepWithValidVerticalStepElementReturnsTrue() {
        for (char c = 'A'; c <= 'Z'; c++) {
            element = new Element(coordinates, c);
            Assert.assertTrue(isValidVerticalStep(element));
        }
        Assert.assertTrue(isValidVerticalStep(new Element(coordinates, '|')));
        Assert.assertTrue(isValidVerticalStep(new Element(coordinates, '+')));
        Assert.assertTrue(isValidVerticalStep(new Element(coordinates, 'x')));
    }

    @Test
    public void isValidHorizontalStepWithNullElementReturnsFalse() {
        Assert.assertFalse(isValidHorizontalStep(element));
    }

    @Test
    public void isValidHorizontalStepWithNonValidHorizontalStepElementReturnsFalse() {
        Assert.assertFalse(isValidHorizontalStep(new Element(coordinates, '|')));
        Assert.assertFalse(isValidHorizontalStep(new Element(coordinates, ' ')));
        Assert.assertFalse(isValidHorizontalStep(new Element(coordinates, '@')));
    }

    @Test
    public void isValidHorizontalStepWithValidHorizontalStepElementReturnsTrue() {
        for (char c = 'A'; c <= 'Z'; c++) {
            element = new Element(coordinates, c);
            Assert.assertTrue(isValidHorizontalStep(element));
        }
        Assert.assertTrue(isValidHorizontalStep(new Element(coordinates, '-')));
        Assert.assertTrue(isValidHorizontalStep(new Element(coordinates, '+')));
        Assert.assertTrue(isValidHorizontalStep(new Element(coordinates, 'x')));
    }
}