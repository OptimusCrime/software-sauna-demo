package com.codehub.traveller.handler;

import com.codehub.traveller.exception.InvalidDirectionException;
import com.codehub.traveller.exception.InvalidElementException;
import com.codehub.traveller.exception.InvalidPathException;
import com.codehub.traveller.exception.MissingStartException;
import com.codehub.traveller.model.Coordinates;
import com.codehub.traveller.model.Direction;
import com.codehub.traveller.model.Element;
import com.codehub.traveller.model.Matrix;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

public class DirectionHandlerTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private DirectionHandler directionHandler;

    @Mock
    private Matrix matrix;

    @Mock
    private Coordinates coordinates;

    @Mock
    private Element element;

    /**
     * Set up.
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = MissingStartException.class)
    public void findInitialDirectionWithNullStartThrowsMissingStartException() {
        directionHandler = new DirectionHandler(matrix);
        Mockito.when(matrix.getStart()).thenReturn(null);
        directionHandler.findInitialDirection();
    }

    @Test
    public void findInitialDirectionWithNoValidDirectionsThrowsInvalidPathException() {
        List<Element> elements = new ArrayList<>();
        coordinates = new Coordinates(1, 1);
        element = new Element(coordinates, '@');
        elements.add(element);
        matrix = new Matrix(elements, element, null);
        directionHandler = new DirectionHandler(matrix);

        expectedException.expect(InvalidPathException.class);
        expectedException.expectMessage("No valid paths found!");

        directionHandler.findInitialDirection();
    }

    @Test
    public void findInitialDirectionWithMultipleDirectionsThrowsInvalidPathException() {
        List<Element> elements = new ArrayList<>();
        coordinates = new Coordinates(1, 1);
        element = new Element(coordinates, '@');
        elements.add(element);
        elements.add(new Element(new Coordinates(0, 1), '|'));
        elements.add(new Element(new Coordinates(1, 0), '-'));
        matrix = new Matrix(elements, element, null);
        directionHandler = new DirectionHandler(matrix);

        expectedException.expect(InvalidPathException.class);
        expectedException.expectMessage("Multiple paths found");

        directionHandler.findInitialDirection();
    }

    @Test
    public void findInitialDirectionWithSingleValidLeftDirectionReturnsDirectionLeft() {
        List<Element> elements = new ArrayList<>();
        coordinates = new Coordinates(1, 1);
        element = new Element(coordinates, '@');
        elements.add(element);
        elements.add(new Element(new Coordinates(0, 1), ' '));
        elements.add(new Element(new Coordinates(1, 0), '-'));
        elements.add(new Element(new Coordinates(1, 2), ' '));
        elements.add(new Element(new Coordinates(2, 1), ' '));
        matrix = new Matrix(elements, element, null);
        directionHandler = new DirectionHandler(matrix);

        Assert.assertEquals(Direction.LEFT, directionHandler.findInitialDirection());
    }

    @Test
    public void findInitialDirectionWithSingleValidRightDirectionReturnsDirectionRight() {
        List<Element> elements = new ArrayList<>();
        coordinates = new Coordinates(1, 1);
        element = new Element(coordinates, '@');
        elements.add(element);
        elements.add(new Element(new Coordinates(0, 1), ' '));
        elements.add(new Element(new Coordinates(1, 0), ' '));
        elements.add(new Element(new Coordinates(1, 2), '-'));
        elements.add(new Element(new Coordinates(2, 1), ' '));
        matrix = new Matrix(elements, element, null);
        directionHandler = new DirectionHandler(matrix);

        Assert.assertEquals(Direction.RIGHT, directionHandler.findInitialDirection());
    }

    @Test
    public void findInitialDirectionWithSingleValidUpDirectionReturnsDirectionUp() {
        List<Element> elements = new ArrayList<>();
        coordinates = new Coordinates(1, 1);
        element = new Element(coordinates, '@');
        elements.add(element);
        elements.add(new Element(new Coordinates(0, 1), '|'));
        elements.add(new Element(new Coordinates(1, 0), ' '));
        elements.add(new Element(new Coordinates(1, 2), ' '));
        elements.add(new Element(new Coordinates(2, 1), ' '));
        matrix = new Matrix(elements, element, null);
        directionHandler = new DirectionHandler(matrix);

        Assert.assertEquals(Direction.UP, directionHandler.findInitialDirection());
    }

    @Test
    public void findInitialDirectionWithSingleValidDownDirectionReturnsDirectionDown() {
        List<Element> elements = new ArrayList<>();
        coordinates = new Coordinates(1, 1);
        element = new Element(coordinates, '@');
        elements.add(element);
        elements.add(new Element(new Coordinates(0, 1), ' '));
        elements.add(new Element(new Coordinates(1, 0), ' '));
        elements.add(new Element(new Coordinates(1, 2), ' '));
        elements.add(new Element(new Coordinates(2, 1), '|'));
        matrix = new Matrix(elements, element, null);
        directionHandler = new DirectionHandler(matrix);

        Assert.assertEquals(Direction.DOWN, directionHandler.findInitialDirection());
    }

    @Test(expected = InvalidElementException.class)
    public void resolveTurnWithCurrentElementNullThrowsInvalidElementException() {
        directionHandler = new DirectionHandler(matrix);
        directionHandler.resolveTurn(null, Direction.DOWN);
    }

    @Test(expected = InvalidDirectionException.class)
    public void resolveTurnWithCurrentDirectionNullThrowsInvalidDirectionException() {
        directionHandler = new DirectionHandler(matrix);
        directionHandler.resolveTurn(element, null);
    }

    @Test
    public void resolveTurnWithNoValidDirectionsThrowsInvalidPathException() {
        List<Element> elements = new ArrayList<>();
        coordinates = new Coordinates(1, 1);
        element = new Element(coordinates, '+');
        elements.add(element);
        matrix = new Matrix(elements, null, null);
        directionHandler = new DirectionHandler(matrix);

        expectedException.expect(InvalidPathException.class);
        expectedException.expectMessage("No valid paths found!");

        directionHandler.resolveTurn(element, Direction.DOWN);
    }

    @Test
    public void resolveTurnWithMultipleDirectionsThrowsInvalidPathException() {
        List<Element> elements = new ArrayList<>();
        coordinates = new Coordinates(1, 1);
        element = new Element(coordinates, '+');
        elements.add(element);
        elements.add(new Element(new Coordinates(1, 0), '-'));
        elements.add(new Element(new Coordinates(1, 2), '-'));
        matrix = new Matrix(elements, element, null);
        directionHandler = new DirectionHandler(matrix);

        expectedException.expect(InvalidPathException.class);
        expectedException.expectMessage("Multiple paths found");

        directionHandler.resolveTurn(element, Direction.DOWN);
    }

    @Test
    public void resolveTurnWithSingleValidDownDirectionReturnsDirectionDown() {
        List<Element> elements = new ArrayList<>();
        coordinates = new Coordinates(1, 1);
        element = new Element(coordinates, '+');
        elements.add(element);
        elements.add(new Element(new Coordinates(0, 1), ' '));
        elements.add(new Element(new Coordinates(1, 0), ' '));
        elements.add(new Element(new Coordinates(1, 2), ' '));
        elements.add(new Element(new Coordinates(2, 1), '|'));
        matrix = new Matrix(elements, element, null);
        directionHandler = new DirectionHandler(matrix);

        Assert.assertEquals(Direction.DOWN, directionHandler.resolveTurn(element, Direction.LEFT));
    }

    @Test
    public void resolveTurnWithSingleValidUpDirectionReturnsDirectionUp() {
        List<Element> elements = new ArrayList<>();
        coordinates = new Coordinates(1, 1);
        element = new Element(coordinates, '+');
        elements.add(element);
        elements.add(new Element(new Coordinates(0, 1), '|'));
        elements.add(new Element(new Coordinates(1, 0), ' '));
        elements.add(new Element(new Coordinates(1, 2), ' '));
        elements.add(new Element(new Coordinates(2, 1), ' '));
        matrix = new Matrix(elements, element, null);
        directionHandler = new DirectionHandler(matrix);

        Assert.assertEquals(Direction.UP, directionHandler.resolveTurn(element, Direction.LEFT));
    }

    @Test
    public void resolveTurnWithSingleValidLeftDirectionReturnsDirectionLeft() {
        List<Element> elements = new ArrayList<>();
        coordinates = new Coordinates(1, 1);
        element = new Element(coordinates, '+');
        elements.add(element);
        elements.add(new Element(new Coordinates(0, 1), ' '));
        elements.add(new Element(new Coordinates(1, 0), '-'));
        elements.add(new Element(new Coordinates(1, 2), ' '));
        elements.add(new Element(new Coordinates(2, 1), ' '));
        matrix = new Matrix(elements, element, null);
        directionHandler = new DirectionHandler(matrix);

        Assert.assertEquals(Direction.LEFT, directionHandler.resolveTurn(element, Direction.UP));
    }

    @Test
    public void resolveTurnWithSingleValidRightDirectionReturnsDirectionRight() {
        List<Element> elements = new ArrayList<>();
        coordinates = new Coordinates(1, 1);
        element = new Element(coordinates, '+');
        elements.add(element);
        elements.add(new Element(new Coordinates(0, 1), ' '));
        elements.add(new Element(new Coordinates(1, 0), ' '));
        elements.add(new Element(new Coordinates(1, 2), '-'));
        elements.add(new Element(new Coordinates(2, 1), ' '));
        matrix = new Matrix(elements, element, null);
        directionHandler = new DirectionHandler(matrix);

        Assert.assertEquals(Direction.RIGHT, directionHandler.resolveTurn(element, Direction.UP));
    }
}