package com.codehub.traveller.handler;

import com.codehub.traveller.exception.InputFileNotFoundException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class InputHandlerTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void getInputMapFromFileWithNullFilenameThrowsInputFileNotFoundException() {
        expectedException.expect(InputFileNotFoundException.class);
        expectedException.expectMessage("File name is null");
        InputHandler.getInputMapFromFile(null);
    }

    @Test(expected = InputFileNotFoundException.class)
    public void getInputMapFromFileWithMissingFileThrowsInputFileNotFoundException() {
        InputHandler.getInputMapFromFile("missing");
    }

    @Test
    public void getInputMapFromFileWithValidFilenameReturnsExpectedString() {
        String expectedResult = "@---A---+\n" +
                "        |\n" +
                "x-B-+   C\n" +
                "    |   |\n" +
                "    +---+";
        Assert.assertEquals(expectedResult, InputHandler.getInputMapFromFile("test/single-valid-map.txt"));
    }

    @Test
    public void getMultipleInputMapsFromFileWithNullFilenameThrowsInputFileNotFoundException() {
        expectedException.expect(InputFileNotFoundException.class);
        expectedException.expectMessage("File name is null");
        InputHandler.getMultipleInputMapsFromFile(null);
    }

    @Test(expected = InputFileNotFoundException.class)
    public void getMultipleInputMapsFromFileWithMissingFileThrowsInputFileNotFoundException() {
        InputHandler.getInputMapFromFile("missing");
    }

    @Test
    public void getMultipleInputMapsFromFileWithValidFilenameReturnsExpectedStringArray() {
        String firstElement = "@---A---+\n" +
                "        |\n" +
                "x-B-+   C\n" +
                "    |   |\n" +
                "    +---+\n";

        String secondElement = "\n@---A---+\n" +
                "        |\n" +
                "x-B-+   C\n" +
                "    |   |\n" +
                "    +---+";
        String[] expectedResult = {firstElement, secondElement};
        Assert.assertArrayEquals(expectedResult, InputHandler.getMultipleInputMapsFromFile("test/multiple-valid-maps.txt"));
    }
}