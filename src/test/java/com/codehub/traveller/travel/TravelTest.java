package com.codehub.traveller.travel;

import com.codehub.traveller.exception.InvalidPathException;
import com.codehub.traveller.factory.MatrixFactory;
import com.codehub.traveller.model.Matrix;
import org.junit.Test;

public class TravelTest {


    @Test(expected = InvalidPathException.class)
    public void travelWithInvalidPathThrowsInvalidPathException() {
        String invalidPathMap = "     --B\n" +
                "       |\n" +
                "@--A---+\n" +
                "       |\n" +
                "  x+   C\n" +
                "   |   |\n" +
                "   +---+";
        Matrix matrix = new MatrixFactory().create(invalidPathMap);
        PathTraveller pathTraveller = new PathTraveller(matrix);
        pathTraveller.initialize().travel();
    }

    @Test
    public void travelWithValidInputCorrectlyTravelsTheMatrix() {
        String invalidPathMap = "@---A---+\n" +
                "        |\n" +
                "x-B-+   C\n" +
                "    |   |\n" +
                "    +---+";
        Matrix matrix = new MatrixFactory().create(invalidPathMap);
        PathTraveller pathTraveller = new PathTraveller(matrix);
        pathTraveller.initialize().travel();
    }
}