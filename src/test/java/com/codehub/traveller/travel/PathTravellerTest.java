package com.codehub.traveller.travel;

import com.codehub.traveller.exception.InvalidPathException;
import com.codehub.traveller.model.Coordinates;
import com.codehub.traveller.model.Element;
import com.codehub.traveller.model.Matrix;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

public class PathTravellerTest {

    private PathTraveller pathTraveller;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private Matrix matrix;

    /**
     * Set up.
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void initializeWithNullMatrixThrowsIllegalArgumentException() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Matrix must not be null!");
        new PathTraveller(null).initialize();
    }

    @Test
    public void initializeWithNoValidDirectionsInMatrixThrowsInvalidPathException() {
        expectedException.expect(InvalidPathException.class);
        expectedException.expectMessage("No valid paths found!");
        List<Element> elements = new ArrayList<>();
        Element element = new Element(new Coordinates(1, 1), '@');
        elements.add(element);
        matrix = new Matrix(elements, element, null);
        pathTraveller = new PathTraveller(matrix);
        pathTraveller.initialize();
    }

    @Test
    public void initializeWithMultipleValidDirectionsInMatrixThrowsInvalidPathException() {
        expectedException.expect(InvalidPathException.class);
        expectedException.expectMessage("Multiple paths found");
        List<Element> elements = new ArrayList<>();
        Element element = new Element(new Coordinates(1, 1), '@');
        elements.add(new Element(new Coordinates(0, 1), '|'));
        elements.add(new Element(new Coordinates(1, 0), '-'));
        elements.add(element);
        matrix = new Matrix(elements, element, null);
        pathTraveller = new PathTraveller(matrix);
        pathTraveller.initialize();
    }

    @Test
    public void initializeCorrectlyReturnsInstanceOfTravel() {
        List<Element> elements = new ArrayList<>();
        Element element = new Element(new Coordinates(1, 1), '@');
        elements.add(new Element(new Coordinates(1, 0), '-'));
        elements.add(element);
        matrix = new Matrix(elements, element, null);
        pathTraveller = new PathTraveller(matrix);
        Assert.assertEquals(Travel.class, pathTraveller.initialize().getClass());
    }

    private void instantiateMatrix() {
        List<Element> elements = new ArrayList<>();
        Coordinates coordinates = new Coordinates(1, 1);
        Element element = new Element(coordinates, '@');
        elements.add(element);
        elements.add(new Element(new Coordinates(0, 1), 'U'));
        elements.add(new Element(new Coordinates(1, 0), 'L'));
        elements.add(new Element(new Coordinates(1, 2), 'R'));
        elements.add(new Element(new Coordinates(2, 1), 'D'));
        matrix = new Matrix(elements, null, null);
    }

}