package com.codehub.traveller.resolver;

import com.codehub.traveller.exception.InvalidCoordinatesException;
import com.codehub.traveller.model.Coordinates;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CoordinatesResolverTest {

    private Coordinates coordinates;

    /**
     * Set up.
     */
    @Before
    public void setUp() {
        coordinates = new Coordinates(1, 1);
    }

    @Test(expected = InvalidCoordinatesException.class)
    public void getLeftAdjacentElementCoordinatesWithNullCoordinatesThrowsInvalidCoordinatesException() {
        CoordinatesResolver.getLeftAdjacentElementCoordinates(null);
    }

    @Test
    public void getLeftAdjacentElementCoordinatesReturnsCoordinates() {
        Coordinates result = CoordinatesResolver.getLeftAdjacentElementCoordinates(coordinates);
        Assert.assertEquals(1, result.getxAxis());
        Assert.assertEquals(0, result.getyAxis());
    }

    @Test(expected = InvalidCoordinatesException.class)
    public void getRightAdjacentElementCoordinatesWithNullCoordinatesThrowsInvalidCoordinatesException() {
        CoordinatesResolver.getRightAdjacentElementCoordinates(null);
    }

    @Test
    public void getRightAdjacentElementCoordinatesReturnsCoordinates() {
        Coordinates result = CoordinatesResolver.getRightAdjacentElementCoordinates(coordinates);
        Assert.assertEquals(1, result.getxAxis());
        Assert.assertEquals(2, result.getyAxis());
    }

    @Test(expected = InvalidCoordinatesException.class)
    public void getUpperAdjacentElementCoordinatesWithNullCoordinatesThrowsInvalidCoordinatesException() {
        CoordinatesResolver.getUpperAdjacentElementCoordinates(null);
    }

    @Test
    public void getUpperAdjacentElementCoordinatesReturnsCoordinates() {
        Coordinates result = CoordinatesResolver.getUpperAdjacentElementCoordinates(coordinates);
        Assert.assertEquals(0, result.getxAxis());
        Assert.assertEquals(1, result.getyAxis());
    }

    @Test(expected = InvalidCoordinatesException.class)
    public void getDownwardsAdjacentElementCoordinatesWithNullCoordinatesThrowsInvalidCoordinatesException() {
        CoordinatesResolver.getDownwardsAdjacentElementCoordinates(null);
    }

    @Test
    public void getDownwardsAdjacentElementCoordinatesReturnsCoordinates() {
        Coordinates result = CoordinatesResolver.getDownwardsAdjacentElementCoordinates(coordinates);
        Assert.assertEquals(2, result.getxAxis());
        Assert.assertEquals(1, result.getyAxis());
    }
}