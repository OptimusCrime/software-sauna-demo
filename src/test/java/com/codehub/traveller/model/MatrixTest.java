package com.codehub.traveller.model;

import com.codehub.traveller.exception.InvalidDirectionException;
import com.codehub.traveller.exception.InvalidElementException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

public class MatrixTest {

    @Mock
    private Matrix matrix;

    @Mock
    private Coordinates coordinates;

    @Mock
    private Element element;

    /**
     * Set up.
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getElementByCoordinatesWithNullCoordinatesReturnsNull() {
        Assert.assertNull(matrix.getElementByCoordinates(null));
    }

    @Test
    public void getElementByNonExistingCoordinatesReturnsNull() {
        Assert.assertNull(matrix.getElementByCoordinates(coordinates));
    }

    @Test
    public void getElementByCoordinatesWithExistingElementReturnsElement() {
        instantiateMatrix();
        Assert.assertEquals(element, matrix.getElementByCoordinates(coordinates));
    }

    @Test(expected = InvalidElementException.class)
    public void getElementForDirectionWithNullElementThrowsInvalidElementException() {
        instantiateMatrix();
        matrix.getElementForDirection(Direction.UP, null);
    }

    @Test(expected = InvalidDirectionException.class)
    public void getElementForDirectionWithNullDirectionThrowsIllegalStateException() {
        instantiateMatrix();
        matrix.getElementForDirection(null, element);
    }

    @Test
    public void getElementForDirectionUpWithNonExistingElementReturnsNull() {
        Assert.assertNull(matrix.getElementForDirection(Direction.UP, element));
    }

    @Test
    public void getElementForDirectionDownWithNonExistingElementReturnsNull() {
        Assert.assertNull(matrix.getElementForDirection(Direction.DOWN, element));
    }

    @Test
    public void getElementForDirectionLeftWithNonExistingElementReturnsNull() {
        Assert.assertNull(matrix.getElementForDirection(Direction.LEFT, element));
    }

    @Test
    public void getElementForDirectionRightWithNonExistingElementReturnsNull() {
        Assert.assertNull(matrix.getElementForDirection(Direction.RIGHT, element));
    }

    @Test
    public void getElementForDirectionUpWithValidInputReturnsElement() {
        instantiateMatrix();
        Element result = matrix.getElementForDirection(Direction.UP, element);
        Assert.assertEquals('U', result.getValue());
        Assert.assertEquals(0, result.getCoordinates().getxAxis());
        Assert.assertEquals(1, result.getCoordinates().getyAxis());
    }

    @Test
    public void getElementForDirectionDownWithValidInputReturnsElement() {
        instantiateMatrix();
        Element result = matrix.getElementForDirection(Direction.DOWN, element);
        Assert.assertEquals('D', result.getValue());
        Assert.assertEquals(2, result.getCoordinates().getxAxis());
        Assert.assertEquals(1, result.getCoordinates().getyAxis());
    }

    @Test
    public void getElementForDirectionLeftWithValidInputReturnsElement() {
        instantiateMatrix();
        Element result = matrix.getElementForDirection(Direction.LEFT, element);
        Assert.assertEquals('L', result.getValue());
        Assert.assertEquals(1, result.getCoordinates().getxAxis());
        Assert.assertEquals(0, result.getCoordinates().getyAxis());
    }

    @Test
    public void getElementForDirectionRightWithValidInputReturnsElement() {
        instantiateMatrix();
        Element result = matrix.getElementForDirection(Direction.RIGHT, element);
        Assert.assertEquals('R', result.getValue());
        Assert.assertEquals(1, result.getCoordinates().getxAxis());
        Assert.assertEquals(2, result.getCoordinates().getyAxis());
    }

    private void instantiateMatrix() {
        List<Element> elements = new ArrayList<>();
        coordinates = new Coordinates(1, 1);
        element = new Element(coordinates, '@');
        elements.add(element);
        elements.add(new Element(new Coordinates(0, 1), 'U'));
        elements.add(new Element(new Coordinates(1, 0), 'L'));
        elements.add(new Element(new Coordinates(1, 2), 'R'));
        elements.add(new Element(new Coordinates(2, 1), 'D'));
        matrix = new Matrix(elements, null, null);
    }
}