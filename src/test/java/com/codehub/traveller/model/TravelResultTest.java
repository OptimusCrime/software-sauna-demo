package com.codehub.traveller.model;

import com.codehub.traveller.exception.InvalidCoordinatesException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class TravelResultTest {

    private TravelResult travelResult;

    @Mock
    private Coordinates coordinates;

    /**
     * Set up.
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        travelResult = new TravelResult();
    }

    @Test(expected = InvalidCoordinatesException.class)
    public void addToVisitedWithNullCoordinatesThrowsInvalidCoordinatesException() {
        travelResult.addToVisited(null);
    }

    @Test()
    public void addToVisitedAddsCoordinatesToVisitedList() {
        travelResult.addToVisited(coordinates);
        Assert.assertEquals(1, travelResult.getVisited().size());
        Assert.assertEquals(coordinates, travelResult.getVisited().get(0));
    }

    @Test()
    public void addToPathAddsCharToPath() {
        travelResult.addToPath('+');
        Assert.assertEquals(1, travelResult.getPath().size());
        Assert.assertEquals(java.util.Optional.of('+').get(), travelResult.getPath().get(0));
    }

    @Test()
    public void addToCollectedLettersAddsLetter() {
        travelResult.addToCollectedLetters('A');
        Assert.assertEquals(1, travelResult.getCollectedLetters().size());
        Assert.assertEquals(java.util.Optional.of('A').get(), travelResult.getCollectedLetters().get(0));
    }
}